#include "Arduino.h"
#include "WiFi.h"
#include "Audio.h"
#include "SD.h"
#include "FS.h"
#include <PCF8575.h>
#include <MFRC522.h>
#include <FastLED.h>
#include <VirtualLed.h>
#include <pattern/BlinkLedColorPattern.h>

// Digital I/O used
#define SD_CS          0
#define SD_MOSI      23
#define SD_MISO      19
#define SD_SCK       18

#define SD_CS          0
#define SD_MOSI      23
#define SD_MISO      19
#define SD_SCK       18


#define I2S_DOUT      25
#define I2S_BCLK      27
#define I2S_LRC       26

#define RFID_RST_PIN 17
#define RFID_SS_PIN  14

#define RELAY_PIN     16

PCF8575 pcf8575(0x20);
MFRC522 mfrc522;
unsigned long last = 0;

#define NUM_LEDS 9
#define LED_PIN 4

const BlinkLedColorPattern BLINKING_GREEN(GREEN, 50);
const SolidLedColorPattern SOLID_RED(RED);

CRGB LED[NUM_LEDS];
VirtualLed baseLeds(0, 7);

unsigned long lastPeriod = 0;
unsigned long lastPeriodValue = 0;

unsigned long getCardId() {
    if (!mfrc522.PICC_ReadCardSerial()) {
        return -1;
    }
    unsigned long hex_num;
    hex_num = mfrc522.uid.uidByte[0] << 24;
    hex_num += mfrc522.uid.uidByte[1] << 16;
    hex_num += mfrc522.uid.uidByte[2] << 8;
    hex_num += mfrc522.uid.uidByte[3];
    return hex_num;
}

void setup() {
    Serial.begin(115200);

    Serial.println(F("Initialize rfid system"));
    //SPI.begin(18,19,23, 5);
    //mfrc522.PCD_Init(5,15);

    SPI.begin(14,12,13,15);
    mfrc522.PCD_Init(15,17);

    Serial.print(F("Reader :"));
    mfrc522.PCD_DumpVersionToSerial();
    FastLED.addLeds<WS2812B, LED_PIN, GRB>(LED, NUM_LEDS);

    for (int i = 0; i < 16; i++) {
        pcf8575.pinMode(i, INPUT);
    }
    pcf8575.begin();
    baseLeds.setPattern((LedColorPattern *) &BLINKING_GREEN);

}
void readCard() {

    if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()){
        Serial.print("Card read:");
        Serial.println(getCardId());
        baseLeds.setPattern((LedColorPattern *)&BLINKING_GREEN);
    } else {
        baseLeds.setPattern((LedColorPattern *)&SOLID_RED);
    }
}

void loop() {
    // audio.loop();
    readCard();
    baseLeds.applyColor(LED);
    FastLED.show();
}

