# Escape room

## Accords de base
- Open source
- Possibilité de documenter le process, release après la salle 
- Support a la création => pas de responsabilité
- Payer des cotisations

## Conseils généraux
- Prendre des lampes zigbee (phillips hue ou ikea pour pouvoir les controller)
- Essayer de rendre les choses le plus simple possible (Kiss) pour que ça soit solide dans le temps
- attention, les objets en verre c'est un risque, si la bouteille qui est cruciale pour une étape casse, il faut un backup.

## Mécanismes 


### Mécanisme d’ouverture de la porte du palier avec boutons on/off

```
Nous souhaitons fixer au mur un panneau/coffret (métal?) d’environ 60 cm x 40 cm.
Sur le panneau seront gravés environ 25 logos, avec sous chaque logo un petit interrupteur on/off.
On aimerait que lorsque la bonne combinaison de cinq interrupteurs (toujours les mêmes) est allumée, la porte qui est située juste à côté se déverrouille (avec un petit bruitage).
```

- Cela nécessite un système pour ouvrir la porte, gâche electronique? 
- baffle + module de son
- peut être fait sans microcontrolleur

### Gravures sur bois

```
On aimerait graver sur 5 plaques de bois différentes des mots (un mot par plaque : “rouge”, “rosé”, “blanc”, “eau”, “raisin”). Il faut que la gravure soit suffisamment profonde/large pour qu’on reconnaisse le mot au seul toucher (à l’aveugle).
```

essayer la gravure

### Mécanisme d’ouverture de la porte avec séquence de couleurs - master mind

```
Nous souhaitons que quelque part dans la pièce apparaisse une séquence de 5 ampoules (ou points lumineux) de couleurs (par exemple : vert, rouge, bleu, jaune, blanc). Celles-ci seront allumées en permanence.

Ailleurs dans la pièce, il y a un présentoir avec également cinq ampoules/points lumineux. 
Lorsque les joueurs entrent dans la pièce, ces ampoules/points lumineux sont allumés et affichent tous la même couleur (par exemple : vert). Devant chaque ampoule/point lumineux, il y a un bouton poussoir. Lorsque les joueurs poussent sur le bouton situé devant une ampoule, celle-ci change de couleur (du  vert on passe au rouge, puis au bleu, puis au jaune, puis au blanc, puis de nouveau au vert, etc.).

NB : Il ne faut pas que cela fasse trop de lumière dans la pièce, qui doit rester sombre. Il faut donc utiliser des ampoules minuscules ou des petits points leds lumineux.

Une fois la bonne combinaison de couleurs trouvée, la porte se déverrouille et s’ouvre avec un cran (idéalement, avec un bruitage).

Idéalement, il faudrait qu’on puisse réinitialiser les couleurs du présentoir de manière automatique une fois la porte ouverte. Retour à la position initiale: toutes les lumières sont vertes.

```

- petites led multicolores.
- intégrer les boutons et les leds?  dans une bouteille?  un bouchon?
- la séquence exemple doit elle être lumineuse, doit elle apparaître lors d'une action



### Table puzzle coulissant

```
Des planches (faces de caisse à vin) sont placées en puzzle coulissant (3X3) sur une table. Il faut faire glisser les plaques pour les mettre au bon endroit.Lorsque le puzzle est reconstitué, le tiroir de la table s’ouvre.

```
identifier les pieces avec hall effect, le plus dur sera la partie mécanique pour de si grande pièces


### Table boussole → Lance un projecteur

```
Sur une table basse ronde est gravée une rose des vents.

Dessus il y a une boîte pour mettre une bouteille de vin.


La boîte tourne sur la table sur différentes positions (NORD, SUD, SUD-OUEST, …).
Les joueurs doivent trouver dans la pièce une bouteille déterminée. Une fois la bonne bouteille de vin replacée dans la boîte et la boîte sur la bonne position pendant au moins 3 secondes (exemple: SUD-OUEST), un projecteur se déclenche et lance une vidéo et la lumière se coupe. 
→ Aimant dans la bouteille ?
→ Comment faire tourner la boîte sur la table ? Est-ce possible que la boîte tourne quand on “actionne un levier/tourne une roue/appuie sur un bouton”? autre part dans la pièce? 

```

- avoir un méchanisme qui actionne la roue rend le tout beaucoup plus compliqué, car il faut absolument protéger la boussole pour ne pas que les gens forcent sinon le méchanisme doit être très très solid.  Protéger est compliqué si il faut insérer la bouteille
- problème de l'aimant, il faut que la bouteille soit insérée dans la bonne orientation, possibilité, mettre un indice qui dit que l'étiquette doit être dansune certaine position ou bande magnétique
- solution simple, une boite avec un goujon en bois et un trou dans la table, un aimant dans la bouteille et un hall dans la table.
- solution compliquée, un méchanisme mais alors on insère pas la bouteille, on doit mettre la bouteille quelque part pour activer le méchanisme


### Chevalet 

```
On place dans la pièce un chevalet à bouteilles de vin (= un double panneau en bois avec des trous pour placer les bouteilles).
Les joueurs doivent replacer une dizaine de bouteilles dans les bons trous.
Une fois toutes les bouteilles replacées à la bonne place, les lumières deviennent sombres, une musique démarre, une bouteille sort d’un tonneau et un hologramme d’une personne apparaît et parle pour féliciter les joueurs.
```

- la complexité ici est d'identifier la bouteille, éventuellement un tag rfid qui identifie les bouteilles avec un tag dans le cul de la bouteille.

### Verre avec image (type verre à saké)

```
Nous souhaiterions mettre au fond d’un verre de vin une image sous une lentille et que cette image ne soit parfaitement visible que lorsque le verre est rempli de liquide. En bref, faire le même principe qu’un verre à saké, mais dans un verre de vin.
Pensez-vous que cela soit possible ?
```

Essayer de sabler (poncer) du verre, l'eau devrait le rendre transparent.  Essayer avec de l'époxy (plus simple a former).  

Ca pourrait être difficile avec un verre à vin car le trou est plus petit que le contenu, comment poncer/sabler facilement le contenu.  
Peut être un verre a champagne?

Attention, utiliser quelque chose de si fragile est-ce une bonne idée?
