#include "Arduino.h"
#include "WiFi.h"
#include "Audio.h"
#include "SD.h"
#include "FS.h"
#include <PCF8575.h>

// Digital I/O used
#define SD_CS          0
#define SD_MOSI      23
#define SD_MISO      19
#define SD_SCK       18

#define I2S_DOUT      25
#define I2S_BCLK      27
#define I2S_LRC       26

#define RELAY_PIN     16
Audio audio;

String ssid = "";
String password = "";

PCF8575 pcf8575(0x20);

int pinWrong = 11;
std::vector<int> good{13,9,10,12,8};

unsigned long last = 0;
bool currentState = 0;

void statusPassed();

void statusFailed();

void statusFailed() {
    digitalWrite(RELAY_PIN, HIGH);
}

void statusPassed() {
    Serial.print("Success! ");
    digitalWrite(RELAY_PIN, LOW);
    //audio.connecttoFS(SD, "/test.mp3");     // SD
}


void setup() {
    Serial.begin(115200);
//    pinMode(SD_CS, OUTPUT);
//    digitalWrite(SD_CS, HIGH);

    pinMode(RELAY_PIN, OUTPUT);
    digitalWrite(RELAY_PIN, HIGH);

//    SPI.begin(SD_SCK, SD_MISO, SD_MOSI);
//    SD.begin(SD_CS);
//
//    audio.setPinout(I2S_BCLK, I2S_LRC, I2S_DOUT);
//    audio.setVolume(21); // default 0...21



    for (int i = 0; i < 16; i++) {
        pcf8575.pinMode(i, INPUT);
    }
    pcf8575.begin();

}

bool isSuccess() {
    if (pcf8575.digitalRead(pinWrong)) {
        return false;
    }
    for (int i : good) {
        if (!pcf8575.digitalRead(i)) {

            return false;
        }
    }
    return true;
}

void loop() {
   // audio.loop();

    if ((millis() - last) > 100) {
        last = millis();
             Serial.print("status ");
             bool success = isSuccess();
             Serial.println(success);
             if(success != currentState){
                 currentState = success;
                 if(success) statusPassed();
                 else statusFailed();

             }

        PCF8575::DigitalInput i = pcf8575.digitalReadAll();
        Serial.printf("0:%i 1:%i 2:%i 3:%i 4:%i 5:%i 6:%i 7:%i 8:%i 9:%i 10:%i 11:%i 12:%i 13:%i 14:%i 15:%i \n", i.p0, i.p1, i.p2, i.p3, i.p4, i.p5, i.p6, i.p7, i.p8, i.p9, i.p10, i.p11, i.p12, i.p13, i.p14, i.p15);
    }
}

