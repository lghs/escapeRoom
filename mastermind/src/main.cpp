#include "Arduino.h"
#include "WiFi.h"
#include "Audio.h"
#include "SD.h"
#include "FS.h"
#include <PCF8575.h>
#include <FastLED.h>
#include <VirtualLed.h>
#include <pattern/BlinkLedColorPattern.h>
#include <Button.h>

// Digital I/O used
#define SD_CS          0
#define SD_MOSI      23
#define SD_MISO      19
#define SD_SCK       18

#define I2S_DOUT      25
#define I2S_BCLK      27
#define I2S_LRC       26

#define RELAY_PIN     16
Audio audio;

String ssid = "";
String password = "";


#define NUM_LEDS 9
#define LED_PIN 4

#define NUM_BUTTONS 4

unsigned long lastResponse = 0;

std::vector<int> states{0, 1, 2, 3};
std::vector<int> correct{0, 1, 2, 3};

Button checkButton(13);

std::vector<Button> buttons{
    Button(17),
    Button(14),
    Button(15),
    Button(12)
};

BlinkLedColorPattern blinkGreen(GREEN, 200);
BlinkLedColorPattern blinkRed(RED, 200);

std::vector<SolidLedColorPattern> patterns{
    SolidLedColorPattern(RED),
    SolidLedColorPattern(GREEN),
    SolidLedColorPattern(BLUE),
    SolidLedColorPattern(CRGB::Chocolate)
};

std::vector<VirtualLed> leds{
    VirtualLed(0, 1),
    VirtualLed(1, 1),
    VirtualLed(2, 1),
    VirtualLed(3, 1)
};

const BlinkLedColorPattern BLINKING_GREEN(GREEN, 50);
const SolidLedColorPattern SOLID_RED(RED);

CRGB LED[NUM_LEDS];


int pinWrong = 11;
std::vector<int> good{13, 9, 10, 12, 8};

unsigned long last = 0;
bool currentState = 0;

void statusPassed();

void statusFailed();

void statusFailed() {
    digitalWrite(RELAY_PIN, HIGH);
}

void statusPassed() {
    Serial.print("Success! ");
    digitalWrite(RELAY_PIN, LOW);
    //audio.connecttoFS(SD, "/test.mp3");     // SD
}


void setup() {
    Serial.begin(115200);


    //    pinMode(SD_CS, OUTPUT);
    //    digitalWrite(SD_CS, HIGH);

    pinMode(RELAY_PIN, OUTPUT);
    digitalWrite(RELAY_PIN, HIGH);

    //    SPI.begin(SD_SCK, SD_MISO, SD_MOSI);
    //    SD.begin(SD_CS);
    //
    //    audio.setPinout(I2S_BCLK, I2S_LRC, I2S_DOUT);
    //    audio.setVolume(21); // default 0...21

    checkButton.setup();
    for (int i = 0; i < buttons.size(); i++) {
        buttons[i].setup();
    }

    FastLED.addLeds<WS2812B, LED_PIN, GRB>(LED, NUM_LEDS);
}

bool isSuccess() {
    for (int i = 0; i < states.size(); i++) {
        if (states[i] != correct[i]) {
            return false;
        }
    }
    return true;
}

void updatePatterns() {
    for (int i = 0; i < states.size(); i++) {
        leds[i].setPattern((LedColorPattern *) &patterns[states[i]]);
    }
}

void applyPatterns() {
    for (int i = 0; i < states.size(); i++) {
        leds[i].applyColor(LED);
    }
}

void applyResponse(bool success) {
    lastResponse = millis();
    for (int i = 0; i < leds.size(); i++) {
        leds[i].setPattern(&(success?blinkGreen:blinkRed));
    }

}

void loop() {
    // audio.loop();

    if (lastResponse == 0 || (millis() - lastResponse) >= 3000) {
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons[i].popClicked()) {
                states[i] = (states[i] + 1) % patterns.size();
            }
        }
        updatePatterns();
        if (checkButton.popClicked()) {
            applyResponse(isSuccess());
        }
    }
    applyPatterns();
    FastLED.show();
}
