#include <Arduino.h>
#include <Button.h>

#define SERVO_DIR 44
#define SERVO_STEP 43
#define SERVO_EN 38

#define SERVO_NRESET 8
#define SERVO_NSLEEP 21
#define SERVO_MS1 48
#define SERVO_MS2 18
#define SERVO_MS3 17

#define DELAY_BETWEEN_STEP 1000

Button b1(4);
Button b2(5);
Button b3(7);

int enabled = 1;
int dir = 0;
int delayBetweenStep = 2000;

void setup() {
    Serial.begin(115200);
    Serial.println("Booted");

    b1.setup();
    b2.setup();
    b3.setup();


    pinMode(SERVO_STEP, OUTPUT);
    pinMode(SERVO_DIR, OUTPUT);

    pinMode(SERVO_EN, OUTPUT);
    pinMode(SERVO_NRESET, OUTPUT);
    pinMode(SERVO_NSLEEP, OUTPUT);
    pinMode(SERVO_MS1, OUTPUT);
    pinMode(SERVO_MS2, OUTPUT);
    pinMode(SERVO_MS3, OUTPUT);

    digitalWrite(SERVO_EN, LOW);
    digitalWrite(SERVO_NRESET, HIGH);
    digitalWrite(SERVO_NSLEEP, HIGH);
    digitalWrite(SERVO_MS1, LOW);
    digitalWrite(SERVO_MS2, LOW);
    digitalWrite(SERVO_MS3, LOW);

}

void loop() {

    if (b1.popClicked()) {
        Serial.println("b1 pressed");
        enabled = !enabled;
    }
    if (b2.popClicked()) {
        Serial.println("b2 pressed");
        dir = !dir;
        digitalWrite(SERVO_DIR, dir);
    }
    if (b3.popClicked()) {
        Serial.println("b3 pressed");
        delayBetweenStep -= 100;
    }


    if (enabled) {
        digitalWrite(SERVO_STEP, HIGH);
        delayMicroseconds(delayBetweenStep);
        digitalWrite(SERVO_STEP, LOW);
        delayMicroseconds(delayBetweenStep);

    }


}