# Table/boussole

Dimensions : 60 x 60 cm maximum (contrainte de la laser)

## Gabarit

- Panneau MDF 122x61 cm **2,5mm**
- Exemple : https://www.hubo.be/fr/p/panneau-mdf-122x61-cm-2-5mm/81045/
- 1 panneau avec ces dimensions permet 2 tentatives

## Plateau sur lequel est dessinée la boussole

- Multiplex 122x61 cm **18mm**
- Exemple : https://www.hubo.be/fr/p/multiplex-122x61-cm-18mm/81023/
- 1 panneau avec ces dimensions permet 2 tentatives

# Puzzle

- Multiplex 122x61 cm **8mm**
- Exemple : https://www.hubo.be/fr/p/multiplex-122x61-cm-8mm/12497/
- Prévoir assez de panneaux pour 3 épaisseurs pour les pièces et le bord + un fond sous l'ensemble du jeu
- A voir si on prend du plus épais pour le fond en fonction de si le puzzle est posé sur du rigide ou pas
